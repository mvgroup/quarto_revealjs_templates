---
title: "Title"
author: "Author"
date: today
format: 
    revealjs:
        theme: white    
        embed-resources: false    
        slide-number: true
        chalkboard: true
        width: 1300
        height: 700        
---


# Topic 1
{{< include presentation-parts/01-topic1.qmd >}}


# Topic 2
{{< include presentation-parts/02-topic2.qmd >}}

