# Quarto reveal.js templates

This repository contains templates to make reveal.js presentations using Quarto

- 01_basic: Make a presentation only with one `index.qmd` file with assets
- 02_intermediate: Make a presentation with one `index.qmd` file with assets and separate presentation parts
- 03_advanced: Make a presentation with one `index.qmd` file with assets, separate presentation parts, and extensions

Feel free to download and use them for your own presentations
